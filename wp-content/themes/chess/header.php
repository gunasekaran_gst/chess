<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <?php wp_head(); ?>
</head>
<body>
<div class="container-fluid header">
    <div class="container">
        <div class="col-lg-12 col-md-12 header col-sm-12 col-xs-12">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="logo">
                    <a href="http://chess.lan/">
                        <img src="http://www.ass-echecs.com/wp-content/uploads/2016/12/logo-ASS-1.png" style="margin-top: 0;" width="90" height="105"> </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="address">
                    <p class="page">The Official Website of</p>
                    <h>UNITED</h>
                    <h>COIMBATORE</h>
                    <yes>CHESS</yes>
                    <no>ASSOCIATION</no>
                </div>
            </div>

            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                <div id="navbar" class="navbar">
                    <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                        <button class="menu-toggle">
                            <?php _e('Menu', 'twentythirteen'); ?>
                        </button>
                        <a class="screen-reader-text  skip-link" href="#content"
                           title="<?php esc_attr_e('Skip to content', 'twentythirteen'); ?>">
                            <?php _e('Skip to content', 'twentythirteen'); ?></a>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'nav-menu',
                                'menu_id' => 'primary-menu',
                            )
                        );
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>




