<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<?php do_action( 'twentythirteen_credits' ); ?>
				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}
				?>

<!--				<a href="--><?php //echo esc_url( __( 'https://wordpress.org/', 'twentythirteen' ) ); ?><!--" class="imprint">-->
<!--					--><?php //printf( __( 'Proudly powered by %s', 'twentythirteen' ), 'WordPress' ); ?>
<!--				</a>-->
			</div>
		</footer>
	</div>
	<?php wp_footer(); ?>

<div class="footer">
    <div class="container">
        <div class="footers1">
            <ul>
                <li><a href="#"> Home </a></li>
                <li><a href="#"> About </a></li>
                <li><a href="#"> Players </a></li>
                <li><a href="#"> Events</a></li>
                <li><a href="#"> Gallery</a></li>
                <li><a href="#"> Coutact</a></li>
            </ul>
        </div>
        <div class="footers2">
            <ul>
                <li><a href="history-of-chess/"> History of chess </a></li>
                <li><a href="ukca-code-of-conduct/"> UKCA code of conduct </a></li>
                <li><a href="ukca-appeal/"> UKCA Appeal </a></li>
                <li><a href="districk-associations/"> Districk Associations</a></li>
                <li><a href="karanataka-coaches/"> Coimbatore Coaches</a></li>
                <li><a href="karnataka-arbiters/"> Coimbatore Arbiters</a></li>
                <li><a href="regulations/">Regulations</a></li>

            </ul>
        </div>

        <div class="footers3">
            <ul>
                <li><a href="player-registration/"> Player Registration</a></li>
                <li><a href="player-search/"> Player Search</a></li>
                <li><a href="karnataka-ims-gms/"> Coimbatore Im's & GMs </a></li>
                <li><a href="results/"> Results</a></li>
                <li><a href="tournaments/"> Tournaments</a></li>
                <li><a href="coaching-camps/"> Coaching Camps</a></li>
                <li><a href="organizing-a-chess-tournament/"> Organizing a Chess Tournament?</a></li>
            </ul>
        </div>

        <div class="footers4">
            United Karnataka Chess Association || 26, Kanteerava Stadium, Kasturba Road, Bangalore – 560001
        </div>
        <div class="footers5">
            Ph - +91 79750  01580 || Email: <a href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin"> <b> gmlinfo@gmail.com </b></a>
        </div>
        <div class="footers6">
        Copyright © 2018 · All Rights Reserved · United Karnataka Chess Association · ||   Website Designed by GML Technologies (P) Ltd. ||   Webmaster
        Total Visitors: 125624
        </div>
    </div>
</div>

</body>
</html>
