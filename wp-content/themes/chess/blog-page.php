<?php
/*
Template Name: Blog Page
*/

get_header();
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php
    while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php the_content(); ?> <!-- Page Content -->
        </div><!-- .entry-content-page -->
    <?php
    endwhile;
    wp_reset_query(); //resetting the page query
    ?>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="text-center">Announcements and News:</h1>
            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1)); ?>
            <?php if ($wpb_all_query->have_posts()) : ?>
                <?php while ($post = $wpb_all_query->have_posts()) : $wpb_all_query->the_post(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <?php if (has_post_thumbnail(get_the_ID())): ?>
                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'medium' ); ?>
                                <img class="post_image" src="<?php echo $image[0]; ?>"/>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>


