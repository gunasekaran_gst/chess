<?php
/*
Template Name: Blog About
*/

get_header();
?><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<!--        --><?php //if (has_post_thumbnail(get_the_ID())): ?>
<!--            --><?php //$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'medium' ); ?>
<!--            <img class="post_image" src="--><?php //echo $image[0]; ?><!--"/>-->
<!--        --><?php //endif; ?>
    </div>
</div>

<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
    <div class="container">
        <?php
        while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
            <div class="entry-content-page">
                <?php the_content(); ?> <!-- Page Content -->
            </div><!-- .entry-content-page -->
        <?php
        endwhile;
        wp_reset_query(); //resetting the page query
        ?>
    </div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php get_footer(); ?>
</div>

